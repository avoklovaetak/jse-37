package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userRepository.findById(userId);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(propertyService, password);
        userRepository.setPassword(userId, hash);
    }

}
