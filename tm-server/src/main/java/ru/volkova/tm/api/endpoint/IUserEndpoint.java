package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.auth.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Optional;

public interface IUserEndpoint {

    @WebMethod
    void setPassword(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

}
