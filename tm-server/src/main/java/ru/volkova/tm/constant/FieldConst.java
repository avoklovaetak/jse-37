package ru.volkova.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface FieldConst {

    @NotNull
    String ID = "id";

    @NotNull
    String NAME = "name";

    @NotNull
    String DESCRIPTION = "description";

    @NotNull
    String STATUS = "status";

    @NotNull
    String DATE_START = "date_start";

    @NotNull
    String DATE_FINISH = "date_finish";

    @NotNull
    String CREATED = "created";

    @NotNull
    String USER_ID = "user_id";

    @NotNull
    String PROJECT_ID = "project_id";

    @NotNull
    String EMAIL = "EMAIL";

    @NotNull
    String FIRST_NAME = "first_name";

    @NotNull
    String LAST_NAME = "last_name";

    @NotNull
    String MIDDLE_NAME = "middle_name";

    @NotNull
    String LOGIN = "login";

    @NotNull
    String LOCKED = "locked";

    @NotNull
    String PASSWORD_HASH = "password_hash";

    @NotNull
    String PHONE = "phone";

    @NotNull
    String ROLE = "role";

    @NotNull
    String TIMESTAMP = "timestamp";

    @NotNull
    String SIGNATURE = "signature";

}
