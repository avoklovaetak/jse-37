package ru.volkova.tm.exception.empty;

import ru.volkova.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty or not found...");
    }

}

