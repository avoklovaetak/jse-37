package ru.volkova.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface TableConst {

    @NotNull
    String TABLE_PROJECT = "project";

    @NotNull
    String TABLE_TASK = "task";

    @NotNull
    String TABLE_SESSION = "session";

    @NotNull
    String TABLE_USER = "user";

}
