package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.ConnectionService;
import ru.volkova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connectionService);

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Task task = new Task();
        Assert.assertNotNull(taskRepository.add(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void addWithParametersTest() {
        final Task task = taskRepository.add("1", "Project 1", "it is project");
        Assert.assertNotNull(task);
        Assert.assertEquals("1", task.getUserId());
        Assert.assertEquals("Project 1", task.getName());
        Assert.assertEquals("it is project", task.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Task> taskList = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskList.add(task1);
        taskList.add(task2);
        taskRepository.addAll(taskList);
        Assert.assertEquals(
                taskRepository.findById(task1.getUserId(),task1.getId()),
                task1);
        Assert.assertEquals(
                taskRepository.findById(task2.getUserId(),task2.getId()),
                task2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findAll(task.getUserId()));
        taskRepository.remove(task);
        Assert.assertTrue(taskRepository.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findAll(task.getUserId()));
        taskRepository.clear(user.getId());
        Assert.assertTrue(taskRepository.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIdTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertEquals(
                taskRepository.findById(task.getUserId(), task.getId()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByIndexTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertEquals(
                taskRepository.findOneByIndex(task.getUserId(), 0),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectByNameTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        task.setName("DEMO");
        taskRepository.add(task);
        Assert.assertEquals(
                taskRepository.findOneByName(task.getUserId(), task.getName()),
                task);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.findById(task.getUserId(), task.getId()));
        taskRepository.removeById(task.getUserId(), task.getId());
        Assert.assertTrue(taskRepository.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        Assert.assertEquals(
                taskRepository.findOneByIndex(task.getUserId(), 0),
                task);
        taskRepository.removeOneByIndex(task.getUserId(), 0);
        Assert.assertTrue(taskRepository.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        final Task task = new Task();
        task.setUserId(user.getId());
        task.setName("DEMO");
        taskRepository.add(task);
        Assert.assertEquals(
                taskRepository.findOneByName(task.getUserId(), task.getName()),
                task);
        taskRepository.removeOneByName(task.getUserId(), task.getName());
        Assert.assertTrue(taskRepository.findAll(task.getUserId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        taskRepository.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        taskRepository.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<Task> taskList = taskRepository.findAllByProjectId(project.getUserId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllByProjectIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        taskRepository.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<Task> taskList = taskRepository.findAllByProjectId(project.getUserId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
        taskRepository.removeAllByProjectId(project.getUserId(), project.getId());
        Assert.assertTrue(taskRepository.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUserId(user.getId());
        taskRepository.add(task);
        taskRepository.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        taskRepository.unbindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

}
