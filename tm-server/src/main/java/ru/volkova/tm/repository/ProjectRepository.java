package ru.volkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.constant.FieldConst;
import ru.volkova.tm.constant.TableConst;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.enumerated.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @NotNull
    @SneakyThrows
    @Override
    protected String getTableName() {
        return TableConst.TABLE_PROJECT;
    }

    @Nullable
    @SneakyThrows
    @Override
    protected Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setDateStart(row.getDate(FieldConst.DATE_START));
        project.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        project.setCreated(row.getDate(FieldConst.CREATED));
        project.setStatus(Status.valueOf(row.getString(FieldConst.STATUS)));
        project.setUserId(row.getString(FieldConst.USER_ID));
        return project;
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project insert(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final String insert =
                "INSERT INTO '" + getTableName() + "'('id','name','description,'" +
                        "'date_start','date_end','created','status','user_id'" +
                        "VALUES (?,?,?,?,?,?,?,?)";
        @NotNull PreparedStatement statement = getConnection().prepareStatement(insert);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setDate(4, prepare(project.getDateStart()));
        statement.setDate(5, prepare(project.getDateFinish()));
        statement.setDate(6, prepare(project.getCreated()));
        statement.setString(7, prepare(project.getStatus()));
        statement.setString(8, project.getUserId());
        statement.execute();
        return project;
    }

    @Override
    public void add(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        insert(project);
    }

}
