package ru.volkova.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractEntity;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private final Connection connection;

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    @Nullable
    protected abstract E fetch(@Nullable final ResultSet row);

    protected abstract String getTableName();

    @NotNull
    @Override
    public Optional<E> add(@NotNull final E entity) {
        entities.add(entity);
        return Optional.of(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

    @Override
    public void addAll(@Nullable final List<E> newEntities){
        if (newEntities == null) throw new ObjectNotFoundException();
        entities.addAll(newEntities);
    }

    @Nullable
    public Date prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new Date(date.getTime());
    }

    @Nullable
    public String prepare(@Nullable final Status status) {
        if (status == null) return null;
        return String.valueOf(status);
    }

    @Nullable
    public Role prepareRole(@Nullable final String role) {
        if (role == null) return null;
        return Role.valueOf(role);
    }

    @Nullable
    public String prepare(@Nullable final Role role) {
        if (role == null) return null;
        return String.valueOf(role);
    }

}
