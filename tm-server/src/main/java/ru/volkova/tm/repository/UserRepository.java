package ru.volkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.constant.FieldConst;
import ru.volkova.tm.constant.TableConst;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.entity.UserNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TableConst.TABLE_USER;
    }

    @Nullable
    @SneakyThrows
    @Override
    protected User fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setFirstName(row.getString(FieldConst.FIRST_NAME));
        user.setMiddleName(row.getString(FieldConst.MIDDLE_NAME));
        user.setSecondName(row.getString(FieldConst.LAST_NAME));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setLocked(row.getBoolean(FieldConst.LOCKED));
        user.setRole(prepareRole(row.getString(FieldConst.ROLE)));
        return user;
    }

    @Nullable
    @SneakyThrows
    @Override
    public User insert(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final String insert =
                "INSERT INTO " + getTableName() + " (id,email,first_name," +
                        "last_name,middle_name,role,locked,login,password_hash) " +
                        "VALUES (?,?,?,?,?,?,?,?,?)";
        @NotNull PreparedStatement statement = getConnection().prepareStatement(insert);
        statement.setString(1, user.getId());
        statement.setString(2, user.getEmail());
        statement.setString(3, user.getFirstName());
        statement.setString(4, user.getSecondName());
        statement.setString(5, user.getMiddleName());
        statement.setString(6, prepare(user.getRole()));
        statement.setBoolean(7, user.getLocked());
        statement.setString(8, user.getLogin());
        statement.setString(9, user.getPasswordHash());
        statement.execute();
        return user;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DROP TABLE '" + getTableName() + "'";
        @NotNull final Statement statement = getConnection().createStatement();
        statement.execute(query);
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final String query = "SELECT * FROM '" + getTableName() + "'";
        @NotNull final Statement statement = getConnection().createStatement();
        @NotNull List<User> entities = new ArrayList<>();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) return null;
        @NotNull final String query = "SELECT * FROM " + getTableName()
                + " WHERE email = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (id.isEmpty()) return null;
        @NotNull final String query = "SELECT * FROM '" + getTableName()
                + "' WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) return null;
        @NotNull final String query = "SELECT * FROM " + getTableName()
                + " WHERE login = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public void lockByEmail(@NotNull String email) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE email = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setString(2, email);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void lockById(@NotNull String id) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull String login) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE login = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setString(2,login);
        statement.execute();
        statement.close();
    }

    @Override
    public void remove(@NotNull final User user) {
        if (user.getId() == null || user.getId().isEmpty()) return;
        removeById(user.getId());
    }

    @Override
    @SneakyThrows
    public void removeByEmail(@NotNull String email) {
        if (email.isEmpty()) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE email = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, email);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        if (id.isEmpty()) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE login = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, login);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void unlockByEmail(@NotNull String email) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE email = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setString(2, email);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void unlockById(@NotNull String id) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull String login) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET locked = ? WHERE login = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setString(2,login);
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    public void setPassword(
            @NotNull final String userId,
            @Nullable final String hash
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET password_hash = ? WHERE user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, hash);
        statement.setString(2,userId);
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET first_name = ?, second_name = ?, middle_name = ?"
                + "WHERE user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, firstName);
        statement.setString(2,secondName);
        statement.setString(3,middleName);
        statement.setString(4,userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    public User createUser(@Nullable User user) {
        user = insert(user);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

}
