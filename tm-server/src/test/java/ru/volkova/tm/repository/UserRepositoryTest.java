package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.ConnectionService;
import ru.volkova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IUserRepository userRepository = new UserRepository(connectionService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userRepository.add(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<User> userList = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userRepository.addAll(userList);
        Assert.assertEquals(userRepository.findById(user1.getId()),
                user1);
        Assert.assertEquals(userRepository.findById(user2.getId()),
                user2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findAll());
        userRepository.remove(user);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }
    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findAll());
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmail() {
        final User user = new User();
        user.setEmail("lala@la.ru");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("test");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
        userRepository.removeById(user.getId());
        Assert.assertNotNull(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByEmail() {
        final User user = new User();
        user.setEmail("lala@la.ru");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
        userRepository.removeByEmail(user.getEmail());
        Assert.assertNotNull(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("test");
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
        userRepository.removeByLogin(user.getLogin());
        Assert.assertNotNull(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockById() {
        final User user= new User();
        userRepository.add(user);
        userRepository.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByEmail() {
        final User user= new User();
        user.setEmail("test@test.ru");
        userRepository.add(user);
        userRepository.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByLogin() {
        final User user= new User();
        user.setLogin("test");
        userRepository.add(user);
        userRepository.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockById() {
        final User user= new User();
        userRepository.add(user);
        userRepository.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
        userRepository.unlockById(user.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByEmail() {
        final User user= new User();
        user.setEmail("test@test.ru");
        userRepository.add(user);
        userRepository.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
        userRepository.unlockByEmail(user.getEmail());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByLogin() {
        final User user= new User();
        user.setLogin("test");
        userRepository.add(user);
        userRepository.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
        userRepository.unlockByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

}
