package ru.volkova.tm.api.service;

import lombok.SneakyThrows;

public interface IAdminService {

    @SneakyThrows
    public void DataBinarySave();

    @SneakyThrows
    public void DataBinaryLoad();

    @SneakyThrows
    public void DataJsonLoad();

    @SneakyThrows
    public void DataJsonSave();

    @SneakyThrows
    public void DataBase64Load();

    @SneakyThrows
    public void DataBase64Save();

    @SneakyThrows
    public void DataJsonLoadFasterxml();

    @SneakyThrows
    public void DataJsonSaveFasterxml();

    @SneakyThrows
    public void DataJsonLoadJaxb();

    @SneakyThrows
    public void DataJsonSaveJaxb();

    @SneakyThrows
    public void DataXmlLoadFasterxml();

    @SneakyThrows
    public void DataXmlSaveFasterxml();

    @SneakyThrows
    public void DataXmlLoadJaxb();

    @SneakyThrows
    public void DataXmlSaveJaxb();

    @SneakyThrows
    public void DataYamlLoadFasterxml();

    @SneakyThrows
    public void DataYamlSaveFasterxml();

}
