package ru.volkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.constant.FieldConst;
import ru.volkova.tm.constant.TableConst;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task insert(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final String insert =
                "INSERT INTO '" + getTableName() + "'('id','name','description,'" +
                "'project_id','date_start','date_end','created','status','user_id'" +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        @NotNull PreparedStatement statement = getConnection().prepareStatement(insert);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getProjectId());
        statement.setDate(5, prepare(task.getDateStart()));
        statement.setDate(6, prepare(task.getDateFinish()));
        statement.setDate(7, prepare(task.getCreated()));
        statement.setString(8, prepare(task.getStatus()));
        statement.setString(9, task.getUserId());
        statement.execute();
        return task;
    }

    @NotNull
    @SneakyThrows
    @Override
    protected String getTableName() {
        return TableConst.TABLE_TASK;
    }

    @Nullable
    @SneakyThrows
    @Override
    protected Task fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setDateStart(row.getDate(FieldConst.DATE_START));
        task.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        task.setCreated(row.getDate(FieldConst.CREATED));
        task.setStatus(Status.valueOf(row.getString(FieldConst.STATUS)));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        return task;
    }

    @Nullable
    @Override
    public Task add(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return insert(task);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET project_id = ? WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, taskId);
        statement.setString(3, userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String query = "SELECT * FROM '" + getTableName() +
                "' WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull List<Task> entities = new ArrayList<>();
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities;
    }

    @Override
    @SneakyThrows
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String query = "DELETE FROM '" + getTableName() +
                "' WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.execute(query);
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET project_id = ? WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, null);
        statement.setString(2, taskId);
        statement.setString(3, userId);
        statement.execute();
        statement.close();
    }

}
