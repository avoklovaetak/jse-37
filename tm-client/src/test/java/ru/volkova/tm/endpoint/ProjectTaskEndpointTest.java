package ru.volkova.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

import java.util.List;

public class ProjectTaskEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void bindTaskByProjectIdTest() {
        Task task = new Task();
        task.setUserId(session.userId);
        endpointLocator.getProjectEndpoint().addProjectByUser(session, "DEMO", "1");
        Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, "DEMO");
        task = endpointLocator.getTaskEndpoint().addTask(session,task);
        endpointLocator.getProjectTaskEndpoint().bindTaskByProjectId(session, project.getId(), task.getId());
        Assert.assertEquals(project.getId(),
                endpointLocator.getTaskEndpoint().finishTaskById(session, task.id).getProjectId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTasksByProjectIdTest() {
        Task task1 = new Task();
        Task task2 = new Task();
        endpointLocator.getProjectEndpoint().addProjectByUser(session, "test", "test");
        Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, "test");
        task1.setUserId(session.userId);
        task1.setProjectId(project.id);
        task2.setUserId(session.userId);
        task2.setProjectId(project.id);
        task1 = endpointLocator.getTaskEndpoint().addTask(session, task1);
        task2 = endpointLocator.getTaskEndpoint().addTask(session, task2);
        final List<Task> taskList = endpointLocator.getProjectTaskEndpoint()
                .findAllTasksByProjectId(session, project.id);
        Assert.assertEquals(task1.id, taskList.get(0).id);
        Assert.assertEquals(task2.id, taskList.get(1).id);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeAllByProjectIdTest() {
        Task task1 = new Task();
        Task task2 = new Task();
        endpointLocator.getProjectEndpoint().addProjectByUser(session, "test", "test");
        Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, "test");
        task1.setUserId(session.userId);
        task1.setProjectId(project.id);
        task2.setUserId(session.userId);
        task2.setProjectId(project.id);
        task1 = endpointLocator.getTaskEndpoint().addTask(session, task1);
        task2 = endpointLocator.getTaskEndpoint().addTask(session, task2);
        endpointLocator.getProjectTaskEndpoint().removeProjectById(session, project.id);
        endpointLocator.getTaskEndpoint().findTaskById(session, task1.id);
        endpointLocator.getTaskEndpoint().findTaskById(session, task2.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unbindTaskByProjectId() {
        Task task = new Task();
        task.setUserId(session.userId);
        endpointLocator.getProjectEndpoint().addProjectByUser(session, "DEMO", "1");
        Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, "DEMO");
        task = endpointLocator.getTaskEndpoint().addTask(session,task);
        endpointLocator.getProjectTaskEndpoint().bindTaskByProjectId(session, project.id, task.id);
        endpointLocator.getProjectTaskEndpoint().unbindTaskByProjectId(session, project.id, task.id);
        Assert.assertNull(endpointLocator.getTaskEndpoint().findTaskById(session, task.id).getProjectId());
    }

}
