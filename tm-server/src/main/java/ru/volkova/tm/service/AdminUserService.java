package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IAdminUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;

public class AdminUserService extends AbstractService<User> implements IAdminUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public AdminUserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.createUser(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        User user = new User();
        user.setEmail(email);
        user.setPasswordHash(password);
        user.setLogin(login);
        user = userRepository.createUser(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        User user = new User();
        user.setPasswordHash(password);
        user.setLogin(login);
        user.setRole(role);
        user = userRepository.createUser(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @Nullable final User user = userRepository.findByEmail(email);
        return user != null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @Nullable final User user = userRepository.findByLogin(login);
        return user != null;
    }

    @Override
    public void lockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.lockByEmail(email);
    }

    @Override
    public void lockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.lockById(id);
    }

    @Override
    public void lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.lockByLogin(login);
    }

    @Override
    public void remove(@Nullable final User user) {
        if (user == null) throw new ObjectNotFoundException();
        userRepository.remove(user);
    }

    @Override
    public void removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.removeByEmail(email);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }


    @Override
    public void unlockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.unlockByEmail(email);
    }

    @Override
    public void unlockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.unlockById(id);
    }

    @Override
    public void unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.unlockByLogin(login);
    }

    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        if (firstName == null || firstName.isEmpty()
                || secondName == null || secondName.isEmpty()
                || middleName == null || middleName.isEmpty())
            throw new UserNotFoundException();
        userRepository.updateUser(userId, firstName, secondName, middleName);
    }

}

