package ru.volkova.tm.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.repository.ProjectRepository;

import static ru.volkova.tm.enumerated.Status.*;

public class ProjectServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IProjectRepository projectRepository = new ProjectRepository(connectionService);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final User user  = new User();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        projectService.add(user.getId(), "DEMO", "project");
        final Project project = projectRepository.findOneByName(user.getId(), "DEMO");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.changeOneStatusById(user.getId(), project.getId(), NOT_STARTED);
        Assert.assertEquals(NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.changeOneStatusByIndex(user.getId(), 0, NOT_STARTED);
        Assert.assertEquals(NOT_STARTED, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeOneStatusByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("test");
        projectService.add(project);
        projectService.changeOneStatusByName(user.getId(), project.getName(), IN_PROGRESS);
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.clear(project.getUserId());
        Assert.assertTrue(projectService.findAll(user.getId()).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        Assert.assertNotNull(projectService.findById(project.getUserId(), project.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneByIndex(project.getUserId(), 0));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("DEMO");
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneByName(project.getUserId(), project.getName()));
    }

    @Test
    @Category(UnitCategory.class)
    public void finishOneById() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.finishOneById(project.getUserId(), project.getId());
        Assert.assertEquals(COMPLETE, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startOneById() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.startOneById(project.getUserId(), project.getId());
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishOneByIndex() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.finishOneByIndex(project.getUserId(), 0);
        Assert.assertEquals(COMPLETE, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startOneByIndex() {
        final Project project = new Project();
        project.setUserId(user.getId());
        projectService.add(project);
        projectService.startOneByIndex(project.getUserId(), 0);
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishOneByName() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("TEST");
        projectService.add(project);
        projectService.finishOneByName(project.getUserId(), project.getName());
        Assert.assertEquals(COMPLETE, project.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startOneByName() {
        final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("TEST");
        projectService.add(project);
        projectService.startOneByName(project.getUserId(), project.getName());
        Assert.assertEquals(IN_PROGRESS, project.getStatus());
    }

}
