package ru.volkova.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.volkova.tm.api.service.*;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.*;
import ru.volkova.tm.endpoint.User;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.empty.EmptyCommandException;
import ru.volkova.tm.exception.system.UnknownArgumentException;
import ru.volkova.tm.repository.CommandRepository;
import ru.volkova.tm.service.*;
import ru.volkova.tm.util.SystemUtil;
import ru.volkova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements EndpointLocator {

    @NotNull
    public final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    public final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    public final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    public final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    public final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    public final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    public final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    public final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    public final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    public final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    public final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    public final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    public final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();

    @NotNull
    public final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @NotNull
    public final CommandRepository commandRepository = new CommandRepository();

    @NotNull
    public final CommandService commandService = new CommandService(commandRepository);

    private Session session = null;

    public void setSession(Session session) {
        this.session = session;
    }

    @Nullable
    public Session getSession() {
        return session;
    }

    public String getUserId(Session session) {
        return session.getUserId();
    }

    @SneakyThrows
    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.volkova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.volkova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initPID() throws Exception {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

//    private void initUsers() {
//        @NotNull final User user = adminUserEndpoint.createUserWithEmail(session,"user1", "test", "user1@mail.ru");
//        user.setId("1");
//        @NotNull final User admin = adminUserEndpoint.createUserWithRole(session,"admin", "pass", Role.ADMIN);
//        admin.setId("2");
//    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new UnknownArgumentException();
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) return;
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(this);
        command.setBootstrap(this);
        commandService.add(command);
    }

    public void run (final String... args) throws Exception {
        initCommands();
        //initUsers();
        initPID();
        @NotNull String command = "";
        if (parseArgs(args)) System.exit(0);
        while (!command.equals("exit")) {
            System.out.println("ENTER COMMAND:");
            command = TerminalUtil.nextLine();
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.err.println("[FAIL]");
            }
        }
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return adminEndpoint;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public ProjectTaskEndpoint getProjectTaskEndpoint() {
        return projectTaskEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
            return userEndpoint;
    }

    @NotNull
    public CommandService getCommandService() {
        return commandService;
    }

}
