package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @SneakyThrows
    @Override
    public Connection getConnection() {
        Class.forName(propertyService.getJdbcDriver());
        @NotNull final String username = propertyService.getJdbcUser();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
