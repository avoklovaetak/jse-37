package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.User;


public interface IUserService extends IService<User> {

    void setPassword(@NotNull String userId, @Nullable String password);

}
