package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Session session = bootstrap.getSession();
        endpointLocator.getProjectEndpoint().addProjectByUser(session, name, description);
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
