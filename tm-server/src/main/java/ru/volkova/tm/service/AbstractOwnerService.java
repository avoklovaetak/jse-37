package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.api.service.IOwnerService;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.empty.EmptyStatusException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E> implements IOwnerService<E> {

    @NotNull
    protected final IOwnerRepository<E> ownerRepository;

    protected AbstractOwnerService(@NotNull IOwnerRepository<E> repository) {
        super(repository);
        this.ownerRepository = repository;
    }

    @Nullable
    @Override
    public E changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        entity = ownerRepository.changeOneStatusById(userId,id,status);
        return entity;
    }

    @Nullable
    @Override
    public E changeOneStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final Status status) {
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @Nullable E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        entity = ownerRepository.changeOneStatusByIndex(userId,index,status);
        return entity;
    }

    @Nullable
    @Override
    public E changeOneStatusByName(
            @NotNull final String userId,
            @Nullable String name,
            @Nullable Status status
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @Nullable E entity = findOneByName(userId, name);
        if (entity == null) throw new ObjectNotFoundException();
        entity = ownerRepository.changeOneStatusByName(userId,name,status);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        ownerRepository.clear(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return ownerRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<E> comparator
    ) {
        if (comparator == null) throw new ObjectNotFoundException();
        return ownerRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public E findById(
            @NotNull final String userId,
            @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Nullable
    @Override
    public E findOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null) throw new IndexIncorrectException();
        return ownerRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E findOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return ownerRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public E finishOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.finishOneById(userId, id);
    }

    @Nullable
    @Override
    public E finishOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.finishOneByIndex(userId,index);
    }

    @Nullable
    @Override
    public E finishOneByName(
            @NotNull final String userId,
            @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = findOneByName(userId, name);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.finishOneByName(userId,name);
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        ownerRepository.removeById(userId, id);
    }

    @Override
    public void removeOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null) throw new IndexIncorrectException();
        ownerRepository.removeOneByIndex(userId, index);
    }

    @Override
    public void removeOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        ownerRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public E startOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.startOneById(userId,id);
    }

    @Nullable
    @Override
    public E startOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.startOneByIndex(userId,index);
    }

    @Nullable
    @Override
    public E startOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable E entity = findOneByName(userId, name);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.startOneByName(userId,name);
    }

    @Nullable
    @Override
    public E updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        return ownerRepository.updateOneById(userId,id,name,description);
    }

    @Nullable
    @Override
    public E updateOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {

        @Nullable E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return ownerRepository.updateOneByIndex(userId,index,name,description);
    }

}
