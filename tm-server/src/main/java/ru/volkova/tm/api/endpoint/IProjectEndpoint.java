package ru.volkova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;
import java.util.Optional;

public interface IProjectEndpoint {

    @SneakyThrows
    @Nullable
    @WebMethod
    Project changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @Nullable
    @WebMethod
    Project changeProjectStatusByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @SneakyThrows
    @Nullable
    @WebMethod
    Project changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    );

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @SneakyThrows
    @NotNull
    @WebMethod
    List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Project finishProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Project finishProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Project finishProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Project startProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    Project startProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @Nullable
    @WebMethod
    Project startProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @Nullable
    @WebMethod
    Project updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @Nullable
    @WebMethod
    Project updateProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

}
