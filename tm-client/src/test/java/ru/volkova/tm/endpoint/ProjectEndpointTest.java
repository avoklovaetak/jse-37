package ru.volkova.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

public class ProjectEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void addProjectByUserTest() {
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, "project", "test");
        Project project = endpointLocator.getProjectEndpoint()
                .findProjectByName(session, "project");
        Assert.assertNotNull(project);
        Assert.assertEquals("project", project.name);
        Assert.assertEquals("test", project.description);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIdTest() {
        final Status status = Status.COMPLETE;
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, "project", "test");
        Project project = endpointLocator.getProjectEndpoint()
                .changeProjectStatusByName(session, "project", status);
        Assert.assertEquals(status, project.status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByNameTest() {
        final Status status = Status.COMPLETE;
        final String name = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, "test");
        Project project = endpointLocator.getProjectEndpoint()
                .changeProjectStatusByName(session, name, status);
        Assert.assertEquals(status, project.status);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getProjectEndpoint()
                .findProjectByName(session, name));
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertNotNull(endpointLocator.getProjectEndpoint()
                .findProjectByName(session, name));
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getProjectEndpoint()
                .finishProjectByName(session, name).status);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void finishProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.COMPLETE, endpointLocator.getProjectEndpoint()
                .finishProjectByName(session, name).status);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getProjectEndpoint()
                .startProjectByName(session, name).status);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void startProjectByNameTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Assert.assertEquals(Status.IN_PROGRESS, endpointLocator.getProjectEndpoint()
                .startProjectByName(session, name).status);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByIdTest(){
        final String name = "test-project";
        final String description = "test-project";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
        endpointLocator.getProjectEndpoint().findProjectByName(session, name);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest(){
        final String name = "test-project1";
        final String description = "test-project1";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
        endpointLocator.getProjectEndpoint().findProjectByName(session, name);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateProjectByIdTest() {
        final String name = "test-project";
        final String upd_name = "test";
        final String description = "test-project";
        final String upd_description = "test";
        endpointLocator.getProjectEndpoint()
                .addProjectByUser(session, name, description);
        Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, name);
        project = endpointLocator.getProjectEndpoint()
                .updateProjectById(session, project.id, upd_name, upd_description);
        Assert.assertEquals(upd_name, project.name);
        Assert.assertEquals(upd_description, project.description);
        endpointLocator.getProjectEndpoint().removeProjectById(session, project.id);
    }

}
