package ru.volkova.tm.api.service;

import lombok.SneakyThrows;
import ru.volkova.tm.dto.Domain;

public interface IBackupService {

    @SneakyThrows
    void load();

    @SneakyThrows
    void save();

    @SneakyThrows
    public Domain getDomain();

    @SneakyThrows
    public void setDomain(final Domain domain);

}
