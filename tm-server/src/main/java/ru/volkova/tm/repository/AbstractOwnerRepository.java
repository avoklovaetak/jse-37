package ru.volkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E> implements IOwnerRepository<E> {

    public AbstractOwnerRepository(Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull String userId) {
        @NotNull final String query = "DROP TABLE '" + getTableName() + "'";
        @NotNull final Statement statement = getConnection().createStatement();
        statement.execute(query);
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull String userId) {
        @NotNull final String query = "SELECT * FROM '" + getTableName() +
                "' WHERE user_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull List<E> entities = new ArrayList<>();
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities;
    }
    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId,@NotNull final Comparator<E> comparator) {
        @NotNull final String query = "SELECT * FROM '" + getTableName() +
                "' WHERE user_id = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull List<E> entities = new ArrayList<>();
        while (resultSet.next()) entities.add(fetch(resultSet));
        statement.close();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    @Nullable
    @SneakyThrows
    @Override
    public E findById(@NotNull final String userId,@NotNull final String id) {
        if (id.isEmpty()) return null;
        @NotNull final String query = "SELECT * FROM '" + getTableName()
                + "' WHERE id = ? AND user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E findOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        if (index < 0) return null;
        @NotNull final String query = "SELECT * FROM '" + getTableName()
                + "' WHERE user_id = ? LIMIT ?,1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E findOneByName(@NotNull final String userId,@NotNull final String name) {
        if (name.isEmpty()) return null;
        @NotNull final String query = "SELECT * FROM '" + getTableName()
                + "' WHERE name = ? AND user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E finishOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE id = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.COMPLETE));
        statement.setString(2, id);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E finishOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE name = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.COMPLETE));
        statement.setString(2, name);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E finishOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE user_id = ? LIMIT ? OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.COMPLETE));
        statement.setString(2, userId);
        statement.setInt(3, index-1);
        statement.setInt(3, index+1);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E startOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE id = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.IN_PROGRESS));
        statement.setString(2, id);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E startOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE name = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.IN_PROGRESS));
        statement.setString(2, name);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E startOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE user_id = ? LIMIT ? OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(Status.IN_PROGRESS));
        statement.setString(2, userId);
        statement.setInt(3, index-1);
        statement.setInt(3, index+1);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @NotNull
    protected final Predicate<E> predicateByUserId(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @SneakyThrows
    @Override
    public void removeById(@NotNull final String userId,@NotNull final String id) {
        if (id.isEmpty()) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE id = ? AND user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        if (index < 0) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE index = ? AND user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeOneByName(final @NotNull String userId, final String name) {
        if (name == null) return;
        @NotNull final String query = "DELETE FROM '" + getTableName()
                + "' WHERE name = ? AND user_id = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Nullable
    @SneakyThrows
    @Override
    public E changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE id = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(status));
        statement.setString(2, id);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE name = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(status));
        statement.setString(2, name);
        statement.setString(3, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E changeOneStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final Status status
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET status = ? WHERE user_id = ? LIMIT ? OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, prepare(status));
        statement.setString(2, userId);
        statement.setInt(3, index-1);
        statement.setInt(4, index-1);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET name = ?, description = ?"
                + " WHERE id = ? AND user_id = ? ";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.setString(4, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E updateOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final String query = "UPDATE TABLE '" + getTableName()
                + "' SET name = ?, description = ?"
                + " WHERE user_id = ? LIMIT ? OFFSET ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setInt(4, index-1);
        statement.setInt(5, index+1);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ObjectNotFoundException();
        statement.close();
        return fetch(resultSet);
    }

}
