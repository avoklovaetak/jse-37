package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Project;

public interface IProjectService extends IOwnerService<Project> {

    void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

}
