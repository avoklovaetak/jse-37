package ru.volkova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ISessionRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.constant.FieldConst;
import ru.volkova.tm.constant.TableConst;
import ru.volkova.tm.entity.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Nullable
    @SneakyThrows
    @Override
    protected Session fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setUserId(row.getString(FieldConst.USER_ID));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Session> add(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final String query =
                "INSERT INTO `app_session`(`id`, `user_id`, `signature`, `timestamp`) " +
                        "VALUES(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setLong(4, session.getTimestamp());
        statement.execute();
        return Optional.of(session);
    }

    @Nullable
    @Override
    protected String getTableName() {
        return TableConst.TABLE_SESSION;
    }

    public SessionRepository(IConnectionService connectionService) {
        super(connectionService.getConnection());
    }

}
