package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IAdminUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class AdminUserServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IUserRepository userRepository = new UserRepository(connectionService);

    private final IAdminUserService adminUserService = new AdminUserService(userRepository, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(adminUserService.add(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<User> userList = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        userList.add(user1);
        userList.add(user2);
        userRepository.addAll(userList);
        Assert.assertEquals(adminUserService.findById(user1.getId()),
                user1);
        Assert.assertEquals(adminUserService.findById(user2.getId()),
                user2);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final User user = new User();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findAll());
        adminUserService.remove(user);
        Assert.assertTrue(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        final User user = new User();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findAll());
        adminUserService.clear();
        Assert.assertTrue(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        final User user = new User();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findById(user.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmail() {
        final User user = new User();
        user.setEmail("lala@la.ru");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByEmail(user.getEmail()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("test");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByLogin(user.getLogin()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        final User user = new User();
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findById(user.getId()));
        adminUserService.removeById(user.getId());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByEmail() {
        final User user = new User();
        user.setEmail("lala@la.ru");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByEmail(user.getEmail()));
        adminUserService.removeByEmail(user.getEmail());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("test");
        adminUserService.add(user);
        Assert.assertNotNull(adminUserService.findByLogin(user.getLogin()));
        adminUserService.removeByLogin(user.getLogin());
        Assert.assertNotNull(adminUserService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockById() {
        final User user= new User();
        adminUserService.add(user);
        adminUserService.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByEmail() {
        final User user= new User();
        user.setEmail("test@test.ru");
        adminUserService.add(user);
        adminUserService.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockByLogin() {
        final User user= new User();
        user.setLogin("test");
        adminUserService.add(user);
        adminUserService.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockById() {
        final User user= new User();
        adminUserService.add(user);
        adminUserService.lockById(user.getId());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockById(user.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByEmail() {
        final User user= new User();
        user.setEmail("test@test.ru");
        adminUserService.add(user);
        adminUserService.lockByEmail(user.getEmail());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockByEmail(user.getEmail());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockByLogin() {
        final User user= new User();
        user.setLogin("test");
        adminUserService.add(user);
        adminUserService.lockByLogin(user.getLogin());
        Assert.assertEquals(false, user.getLocked());
        adminUserService.unlockByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

}
